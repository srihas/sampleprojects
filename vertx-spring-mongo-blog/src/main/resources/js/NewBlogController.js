(function(){
	var app = angular.module("blogViewer");
	
	var NewBlogController = function($scope,$location,$routeParams,$http,AuthenticationService,userService) {
		
		$scope.title;
		$scope.tags;
		$scope.content;
		
		$scope.postBlog = function(){
			$scope.message = "";
			
			var blog = {
					title : $scope.title,
					tags : $scope.tags,
					content : $scope.content
			}
			
			$http.post('/api/blog/create',blog)
				.then(function(response){
					$scope.message = "Blog posted successfully !"
				});
		}
		
	};
	
	app.controller("NewBlogController",NewBlogController);
	
}());