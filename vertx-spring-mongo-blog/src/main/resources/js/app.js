(function(){
	var app = angular.module("blogViewer",["ngRoute","ngStorage"]);
	
	app.config(function($routeProvider){
		$routeProvider
			.when("/home", {
				templateUrl:"/main/blogs.html",
				controller:"MainController"
			})
			.when("/interests", {
				templateUrl:"/main/blogs.html",
				controller:"MainController"
			})
			.when("/myblogs", {
				templateUrl:"/main/blogs.html",
				controller:"MainController"
			})
			.when("/newblog", {
				templateUrl:"/main/newblog.html",
				controller:"NewBlogController"
			})
			.when("/search", {
				templateUrl:"/main/search.html",
				controller:"MainController"
			})
			.when("/profile", {
				templateUrl:"/main/profile.html",
				controller:"ProfileController"
			})
			.when("/login", {
				templateUrl:"/main/loginform.html",
				controller:"LoginController"
			})
			.when("/register",{
				templateUrl:"/main/registerform.html",
				controller:"RegistrationController"
			})
			.otherwise({
		        redirect: '/home'
		    });
	});
	
}());