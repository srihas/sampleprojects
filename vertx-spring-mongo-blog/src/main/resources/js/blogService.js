(function(){
	
	var blogService = function($http) {
		
		var getBlogs = function() {
			return $http.get("/api/blogs/all")
					.then(function(response) {
						return response.data;
					});
		};
		
		var getMyInterests = function() {
			return $http.get("/api/blogs/myinterests")
					.then(function(response) {
						return response.data;
					});
		};
		
		var getMyBlogs = function() {
			return $http.get("/api/blogs/my")
					.then(function(response) {
						return response.data;
					});
		};
		
		return {
			getBlogs:getBlogs,
			getMyInterests:getMyInterests,
			getMyBlogs:getMyBlogs,
		};
	};
	
	var app = angular.module("blogViewer");
	app.factory("blogService",blogService); 
	
}());