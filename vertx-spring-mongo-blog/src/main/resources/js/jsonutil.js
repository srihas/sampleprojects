/**
 * 
 */
(function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value.trim() || '');
            } else {
                o[this.name] = this.value.trim() || '';
            }
        });
        return o;
    };
    
})(jQuery);

Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

function timeStampInString(UNIX_timestamp) {
	var a = new Date(UNIX_timestamp);
	var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
	var year = a.getFullYear();
	var month = months[a.getMonth()];
	var date = a.getDate();
	
	var hours = a.getHours();
	var minutes = a.getMinutes();
	var ampm = hours >= 12 ? 'PM' : 'AM';
	hours = hours % 12;
	hours = hours ? hours : 12; // the hour '0' should be '12'
	minutes = minutes < 10 ? '0'+minutes : minutes;
	//August 24, 2013 at 9:00 PM
	var time = month+' '+date+', '+year+' at '+hours + ':' + minutes + ' ' + ampm;
	return time;
}

function getTags(options,handleData) {
	$.getJSON( "/api/blog/tags",options,function( data ) {
		handleData(data);
	});
}


