(function(){
	var app = angular.module("blogViewer");
	
	var RegistrationController = function($scope,$window,$location,AuthenticationService,$http) {
		
		$scope.login = function() {
			AuthenticationService.Login($scope.userName, $scope.password, function (result) {
              if (result === true) {
            	  $scope.message = "Login Successful!";
            	  $scope.error = "";
            	  //$window.location.reload();
              } else {
            	  $scope.message = "";
            	  $scope.error="Invalid credentials...Please register incase you haven't!!";
              }
          });
		}
		
		$scope.loadTags = function(query) {
		    return $http.get('/api/blog/tags');
		};
		
		$scope.isUserNameAvailable = function(name) {
			if(typeof name != 'undefined' && name.length>=6) {
				$http.get('/api/user/available/'+name)
					.then(function(response){
						if(response.data==="false") {
							$scope.usernameavlmsg = "Username already taken for "+name;
							$scope.userNameAvailable = false;
						} else {
							$scope.usernameavlmsg = "Username available";
							$scope.userNameAvailable = true;
						}
					});
			}
		}
		
		$scope.formHasErrors;
		$scope.errors = [];
		function isFormValid() {
			var formHasErrors;
			var errors = [];
			
			if($scope.firstName=="") {
				formHasErrors = true;
				errors.push("Please enter first name");
			} 
			
			if($scope.userName.length<6) {
				formHasErrors = true;
				errors.push("Username must have minimum 6 characters");
			} else if(!$scope.userNameAvailable) {
				formHasErrors = true;
				errors.push("Username not available");
			}
			
			var passwd = $scope.password;
			var confirmpasswd = $scope.confirmpassword;
			
			if(passwd == "") {
				formHasErrors = true;
				errors.push("Password cannot be blank");
			} else if(passwd!==confirmpasswd) {
				formHasErrors = true;
				errors.push("Password and Confirm Password are not same");
			}
			
			$scope.errors = errors;
			$scope.formHasErrors = formHasErrors;
			
			if(formHasErrors) {
				$scope.signupalertp = "Please correct errors before registering";
				return false;
			} else {
				return true;		
			}
		}
		
		var timer;
		
		function redirectToPage(element,message,redirectAction){
		    if(timer > 0){
		    	timer--;
		        element = message.replace("{timer}",timer);
		        setTimeout("redirectToPage(\'"+element+"\',\'"+message+"\',"+redirectAction+")", 1000);
		    }else{
		    	timer=2;
		    	redirectAction();
		    }
		}
		
		function redirectToLogin() {
			$location.path("/login");
		}
		
		$scope.register = function() {
			
			if(isFormValid()) {
				var registrationForm = {
						firstName : $scope.firstName,
						lastName : $scope.lastName,
						userName : $scope.userName,
						password : $scope.password,
						email : $scope.email,
						phoneNumber : $scope.phoneNumber,
				}
				
				$http.post('/api/user/register',registrationForm)
					.then(function(response){
						if(response.data=="true") {
							$scope.registrationmsg = "User registration successfull";
						}
					});
			}
		}
		
		
	};
	
	app.controller("RegistrationController",RegistrationController);
	
}());