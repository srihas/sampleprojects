(function(){
	
	var userService = function($http) {
		
		function getUserInfo() {
        	return $http.get('/api/user/info')
            .then(function(response) {
            	var data = response.data;
                return data;
            });
        }
		
		function getFavBlogs() {
        	return $http.get('/api/blogs/fav')
            .then(function(response) {
            	var data = response.data;
                return data;
            });
        }
		
		return {
			getUserInfo:getUserInfo,
			getFavBlogs:getFavBlogs
		};
	};
	
	var app = angular.module("blogViewer");
	app.factory("userService",userService); 
	
}());