(function(){
	var app = angular.module("blogViewer");
	
	var navcontroller = function($scope,$window,$location,AuthenticationService,userService) {
		
		$scope.msg = "Navigation controller";
		
		$scope.isAuthenticated = AuthenticationService.isAuthenticated();
		
		$scope.isActive = function (viewLocation) { 
	        return viewLocation === $location.path();
	    };
		
		if($scope.isAuthenticated) {
			userService.getUserInfo().then(function(data){
				$scope.user = data;
			});
		}
		
		$scope.logout = function() {
			AuthenticationService.Logout();
			//$window.location.reload();
		}
		
		$location.path("/home");
			
	};
	
	app.controller("navcontroller",navcontroller);
	
}());