(function(){
	var app = angular.module("blogViewer");
	
	var LoginController = function($scope,$window,$location,AuthenticationService) {
		
		$scope.login = function() {
			AuthenticationService.Login($scope.userName, $scope.password, function (result) {
              if (result === true) {
            	  $scope.message = "Login Successful!";
            	  $scope.error = "";
            	  //$window.location.reload();
              } else {
            	  $scope.message = "";
            	  $scope.error="Invalid credentials...Please register incase you haven't!!";
              }
          });
		}
		
	};
	
	app.controller("LoginController",LoginController);
	
}());