(function(){
	var app = angular.module("blogViewer");
	
	var MainController = function($scope,$location,$http,blogService,AuthenticationService,userService) {
		
		var path = $location.path();
		
		$scope.favBlogs = [];
		
		var onError = function(reason) {
			$scope.error = "Could not fetch the data."+reason;
		};
		
		var onBlogsLoad = function(data) {
			$scope.allBlogs = data;
		};
		
		var onFavBlogsLoad = function(data) {
			$scope.favBlogs = data;
			loadContent();
		}
		
		$scope.isAuthenticated = AuthenticationService.isAuthenticated();
		
		if($scope.isAuthenticated) {
			userService.getFavBlogs().then(onFavBlogsLoad, onError);
		} else {
			loadContent();
		}
		
		$scope.isFavBlog = function(index) {
			return $.inArray($scope.allBlogs[index].id, $scope.favBlogs)>=0;
		}
		
		$scope.toggleFav = function(index) {
			var blogId = $scope.allBlogs[index].id;
			var favorite = !($.inArray(blogId, $scope.favBlogs)>=0);
			var obj = {
					blogId : blogId,
					favorite : favorite
			}
			$http.post('/api/blog/update',obj)
				.then(function(response){
					if(favorite) {
						$scope.favBlogs.push(blogId);
					} else {
						$scope.favBlogs.remove(blogId);
					}
				});
		}
		
		$scope.postComment = function(index) {
			var comment = $scope.allBlogs[index].comment;
			if(comment.trim()!="") {
				var obj = {
						blogId : $scope.allBlogs[index].id,
						content : comment
				}
				$http.post('/api/blog/comment',obj)
					.then(function(response){
						$scope.allBlogs[index].comment = "";
						if(typeof $scope.allBlogs[index].comments === "undefined") {
							$scope.allBlogs[index].comments = [];
						}
						$scope.allBlogs[index].comments.unshift(response.data);
					});
			}
		}
		
		function loadContent() {
			if(path=='/' || (path=='/login' && $scope.isAuthenticated)) {
				$location.path('/home');
			} else if(path=="/home") {
				blogService.getBlogs().then(onBlogsLoad, onError);
			} else if(path=='/interests') {
				blogService.getMyInterests().then(onBlogsLoad, onError);
			} else if(path=='/myblogs') {
				blogService.getMyBlogs().then(onBlogsLoad, onError);
			}
		}
		
	};
	
	app.controller("MainController",MainController);
	
}());