(function () {
    'use strict';

    angular
        .module('blogViewer')
        .factory('AuthenticationService', Service);

    function Service($http,$localStorage) {
        var service = {};

        service.Login = Login;
        service.Logout = Logout;
        service.isAuthenticated = isAuthenticated;
        
        return service;
        
        var existingUser;

        function Login(userName, password, callback) {
            $http.post('/login', { userName: userName, password: password })
                .then(function(response) {
                	var data = response.data;
                	console.log(' data: ' + data);
                    // login successful if there's a token in the response
                    if (data.token) {
                        // store username and token in local storage to keep user logged in between page refreshes
                    	$localStorage.currentUser = { username: userName, token: data.token };

                        // add jwt token to auth header for all requests made by the $http service
                        $http.defaults.headers.common.Authorization = 'Bearer ' + data.token;
                        
                        existingUser = true;

                        // execute callback with true to indicate successful login
                        callback(true);
                    } else {
                    	
                    	existingUser = false;
                        // execute callback with false to indicate failed login
                        callback(false);
                    }
                });
        }
        
        function isAuthenticated() {
        	//console.log($localStorage.currentUser);
        	if($localStorage.currentUser) {
        		// add jwt token to auth header for all requests made by the $http service
                $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.currentUser.token;
                existingUser = true;
        	}
        	return existingUser;
        }
        
        function Logout() {
            // remove user from local storage and clear http auth header
            delete $localStorage.currentUser;
            $http.defaults.headers.common.Authorization = '';
        }
        
        isAuthenticated();
    }
})();