(function(){
	var app = angular.module("blogViewer");
	
	var ProfileController = function($scope,$http,userService) {
		
		$scope.loadTags = function(query) {
		    return $http.get('/api/blog/tags');
		};
		
		userService.getUserInfo().then(function(data){
				$scope.user = data;
				$scope.confirmpassword = $scope.user.password;
		});
		
		$scope.formHasErrors;
		$scope.errors = [];
		function isFormValid() {
			var formHasErrors;
			var errors = [];
			
			if($scope.user.firstName=="") {
				formHasErrors = true;
				errors.push("Please enter first name");
			} 
			
			var passwd = $scope.user.password;
			var confirmpasswd = $scope.confirmpassword;
			
			if(passwd == "") {
				formHasErrors = true;
				errors.push("Password cannot be blank");
			} else if(passwd!==confirmpasswd) {
				formHasErrors = true;
				errors.push("Password and Confirm Password are not same");
			}
			
			$scope.errors = errors;
			$scope.formHasErrors = formHasErrors;
			
			if(formHasErrors) {
				$scope.signupalertp = "Please correct errors before registering";
				return false;
			} else {
				return true;		
			}
		}
		
		$scope.updateProfile = function() {
			
			if(isFormValid()) {
				$http.post('/api/user/update',$scope.user)
					.then(function(response){
					if(response.data=="true") {
						$scope.message = "Updated profile successfully";
					} else {
						$scope.errormsg = "Updating failed due to some error.";
					}
				});
			}
		}
		
	};
	
	app.controller("ProfileController",ProfileController);
	
}());