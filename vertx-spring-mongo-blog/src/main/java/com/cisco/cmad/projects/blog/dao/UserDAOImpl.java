package com.cisco.cmad.projects.blog.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.UpdateOperations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cisco.cmad.projects.blog.model.Blog;
import com.cisco.cmad.projects.blog.model.User;
import com.mongodb.WriteResult;

@Service
public class UserDAOImpl extends BasicDAO<User, Integer> implements UserDAO {
	
	private static final String PASSWORD_FIELD = "password";

	private static final String USER_NAME = "userName";
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	public UserDAOImpl(Class<User> entityClass, Datastore ds) {
		super(entityClass, ds);
	}
	
	@Override
	public boolean createUser(User user) {
		boolean isSuccess = false;
		if(isUserIdAvailable(user.getUserName())) {
			save(user);
			isSuccess = true;
		} else {
			logger.info("User exists with name : "+user.getUserName());
		}
		return isSuccess;
	}
	
	@Override
	public boolean deleteUser(String userId) {
		WriteResult result = deleteByQuery(createQuery().field(USER_NAME).equal(userId));
		return result.wasAcknowledged();
	}
	
	@Override
	public boolean deleteAllUsers() {
		WriteResult result = deleteByQuery(createQuery());
		return result.wasAcknowledged();
	}
	
	@Override
	public Optional<User> getUserById(String userId) {
		User user = createQuery().field(USER_NAME).equal(userId).get();
		return user!=null?Optional.of(user):Optional.empty();
	}

	@Override
	public List<User> getAllUsers() {
		return createQuery().asList();
	}
	
	@Override
	public List<String> getAllRegisteredUserNames() {
		return getCollection().distinct(USER_NAME);
	}
	
	@Override
	public boolean isUserIdAvailable(String userId) {
		return createQuery().field(USER_NAME).equal(userId).count()==0;
	}

	@Override
	public long getUserCount() {
		return count();
	}

	@Override
	public boolean isValid(String userName, String password) {
		return createQuery().field(USER_NAME).equal(userName).field(PASSWORD_FIELD).equal(password).count()==1;
	}

	@Override
	public List<Blog> getFavouriteBlogs() {
		return new ArrayList<>();
	}

	@Override
	public boolean updateUser(User user) {

		UpdateOperations<User> updateOptions = createUpdateOperations();
		updateOptions.set("firstName", user.getFirstName()).set("lastName", user.getLastName())
				.set(PASSWORD_FIELD, user.getPassword()).set("email", user.getEmail())
				.set("phoneNumber", user.getPhoneNumber()).set("areaOfInterest", user.getAreaOfInterest());

		update(createQuery().field(USER_NAME).equal(user.getUserName()), updateOptions);

		return true;
	}
	
	@Override
	public boolean setFavoriteBlog(String userName, String blogId, boolean favorite) {
		UpdateOperations<User> updateOptions;
		if(favorite) {
			updateOptions = createUpdateOperations().push("favouriteBlogs",blogId);
		} else {
			updateOptions = createUpdateOperations().removeAll("favouriteBlogs",blogId);
		}
		update(createQuery().field(USER_NAME).equal(userName), updateOptions);
		return true;
	}

}
