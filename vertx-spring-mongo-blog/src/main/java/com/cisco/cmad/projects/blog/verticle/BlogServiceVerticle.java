package com.cisco.cmad.projects.blog.verticle;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.cisco.cmad.projects.blog.dao.BlogDAO;
import com.cisco.cmad.projects.blog.model.Blog;
import com.cisco.cmad.projects.blog.model.Comment;
import com.cisco.cmad.projects.blog.model.User;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

@Service
@Qualifier("blogServiceVerticle")
public class BlogServiceVerticle extends AbstractVerticle {
	
	private static final String FAVOURITE_BLOGS = "favouriteBlogs";

	private static final String BLOGS = "Blogs : ";

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private BlogDAO blogDao;
	
	@Override
	public void start() throws Exception {
		vertx.eventBus().consumer("blog.create", message -> {
			logger.info("Received request to create blog: "+message.body().toString());
			Blog blog = Json.decodeValue(message.body().toString(), Blog.class);
			boolean isSuccess;
			isSuccess = blogDao.createBlog(blog);
			
			message.reply(isSuccess);
		});
		
		vertx.eventBus().consumer("blog.list", message -> {
			logger.info("Getting all blogs");
			message.reply(Json.encodePrettily(blogDao.getAllBlogs()));
		});
		
		vertx.eventBus().consumer("blog.add.comment", message -> {
			String jsonString = message.body().toString();
			logger.info("Received request to add comment: "+jsonString);
			JsonObject jsonObject = new JsonObject(jsonString);
			String blogId = (String)jsonObject.getValue("blogId");
			String content = (String)jsonObject.getValue("content");
			String userName = (String)jsonObject.getValue("userName");
			
			Comment comment = new Comment(userName, content);
			boolean isSuccess = blogDao.addComment(blogId,comment);
			JsonObject response = new JsonObject();
			if(isSuccess) {
				response.put("status", "success");
				response.put("data", Json.encode(comment));
			} else {
				response.put("status", "error");
			}
			message.reply(response);
		});
		
		vertx.eventBus().consumer("blog.search", message -> {
			List<String> queryList = new JsonArray(message.body().toString()).getList();
			logger.info("Search blogs by query : "+queryList);
			List<Blog> blogs = blogDao.getBlogsByTagOrTitle(queryList);
			
			blogs.stream().forEach(blog -> {
				if(blog.getContent().length()>100) {
					blog.setContent(blog.getContent().substring(0, 100)+"...");
				}
			});
			
			logger.info(BLOGS+blogs);
			message.reply(Json.encodePrettily(blogs));
		});
		
		vertx.eventBus().consumer("user.blog.list", message -> {
			JsonObject obj = new JsonObject(message.body().toString());
			String userName = (String) obj.getValue("userName");
			logger.info("Getting all blogs created by user : "+userName);
			message.reply(Json.encodePrettily(blogDao.getBlogsByUserId(userName)));
		});
		
		vertx.eventBus().consumer("blog.list.tags", message -> {
			JsonObject obj = new JsonObject(message.body().toString());
			List<String> tags = ((JsonArray) obj.getValue("tags")).getList();
			logger.info("Getting all blogs based on tags : "+tags);
			message.reply(Json.encodePrettily(blogDao.getBlogsByTags((String[]) tags.toArray(new String[0]))));
		});
		
		vertx.eventBus().consumer("blog.list.id", message -> {
			JsonObject obj = new JsonObject(message.body().toString());
			String blogId = (String) obj.getValue("blogId");
			List<String> favoriteBlogs = ((JsonArray) obj.getValue(FAVOURITE_BLOGS)).getList();
			logger.info("Getting blog by id : "+blogId);
			Optional<Blog> blogOpt = blogDao.getBlogById(blogId);
			
			Blog blog = null;
			if(blogOpt.isPresent()) {
				blog = blogOpt.get();
				if(null!=favoriteBlogs && favoriteBlogs.contains(blog.getId())) {
					blog.setFavorite(true);
				}
			}
			
			logger.info("Blog result is "+blog);
			message.reply(Json.encodePrettily(blog));
		});
		
		vertx.eventBus().consumer("blog.list.fav", message -> {
			User user = Json.decodeValue(message.body().toString(), User.class);
			logger.info("Getting all favorite blogs created by user : "+user.getUserName());
			List<String> blogIds = user.getFavouriteBlogs();
			
			List<Blog> blogs = blogDao.getAllBlogs().stream().filter(blog -> blogIds.contains(blog.getId())).collect(toList());
			
			logger.info(BLOGS+blogs);
			message.reply(Json.encodePrettily(blogs));
		});
	}

}
