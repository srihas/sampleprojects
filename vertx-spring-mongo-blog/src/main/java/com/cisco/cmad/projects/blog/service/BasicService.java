package com.cisco.cmad.projects.blog.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class BasicService {

	public List<String> getTags() {
		List<String> tags = new ArrayList<>();
		tags.add("Technology");
		tags.add("Sports");
		tags.add("Movies");
		tags.add("Music");
		
		return tags;
	}
}
