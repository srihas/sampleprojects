package com.cisco.cmad.projects.blog.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cisco.cmad.projects.blog.dao.UserDAO;
import com.cisco.cmad.projects.blog.model.User;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

@Service
public class LoginService {
	
	@Autowired
	private UserDAO userDao;
	
	public boolean isValid(String userName, String password) {
		return userDao.isValid(userName, password);
	}
	
	public User getCurrentUser(RoutingContext rctx) {
		JsonObject principal = rctx.user().principal();
		String userName = principal.getString("userName");
		Optional<User> userOptional = userDao.getUserById(userName);
		return userOptional.isPresent()?userOptional.get():null;
	}
}
