package com.cisco.cmad.projects.blog.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Criteria;
import org.mongodb.morphia.query.PushOptions;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cisco.cmad.projects.blog.model.Blog;
import com.cisco.cmad.projects.blog.model.Comment;
import com.mongodb.WriteResult;

@Service
public class BlogDAOImpl extends BasicDAO<Blog, Integer> implements BlogDAO {
	
	private static final String ORDER_DESC_CREATED_DATE = "-createdDate";

	private static final String TITLE = "title";

	private static final String USER_NAME = "userName";
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	public BlogDAOImpl(Class<Blog> entityClass, Datastore ds) {
		super(entityClass, ds);
	}

	@Override
	public List<Blog> getAllBlogs() {
		return createQuery().order(ORDER_DESC_CREATED_DATE).asList();
	}
	
	@Override
	public List<Blog> getBlogsByIds(List<String> blogIds) {
		return createQuery().field("_id").hasAnyOf(blogIds).order(ORDER_DESC_CREATED_DATE).asList();
	}

	@Override
	public List<Blog> getBlogsByUserId(String userId) {
		return createQuery().field(USER_NAME).equal(userId).order(ORDER_DESC_CREATED_DATE).asList();
	}
	
	@Override
	public List<Blog> getFavouriteBlogs(String userId) {
		return createQuery().field(USER_NAME).equal(userId).field("favorite").equal(Boolean.TRUE).order(ORDER_DESC_CREATED_DATE).asList();
	}

	@Override
	public void setFavourite(String blogId, boolean favourite) {
		Query<Blog> query = createQuery().field("_id").equal(blogId);
		logger.info("Found blog with id ["+blogId+"] as "+query.get());
		UpdateOperations<Blog> ops = createUpdateOperations().set("favorite", true);
		update(query, ops);
	}

	@Override
	public List<Blog> getBlogsByTitle(String title, String userId) {
		return createQuery().field(TITLE).contains(title).field(USER_NAME).equal(userId).order(ORDER_DESC_CREATED_DATE).asList();
	}
	
	@Override
	public List<Blog> searchBlogsByTitle(String title, String userId) {
		return createQuery().field(TITLE).contains(title).field(USER_NAME).equal(userId). project(TITLE,true).order(ORDER_DESC_CREATED_DATE).asList();
	}

	@Override
	public boolean deleteAllBlogs() {
		WriteResult result = deleteByQuery(createQuery());
		return result.wasAcknowledged();
	}
	
	@Override
	public boolean deleteBlogById(String id) {
		WriteResult result = deleteByQuery(createQuery().field("_id").equal(id));
		return result.wasAcknowledged();
	}

	@Override
	public boolean createBlog(Blog blog) {
		Key<Blog> save = save(blog);
		return null!=save;
	}

	@Override
	public Optional<Blog> getBlogById(String id) {
		Blog blog = createQuery().field("_id").equal(id).get();
		return blog!=null?Optional.of(blog):Optional.empty();
	}

	@Override
	public List<Blog> getBlogsByTags(String... tags) {
		Map<String, List<String>> queries = new HashMap<>();
		queries.put("tags", new ArrayList<String>(Arrays.asList(tags)));
		return fetchDocs(queries);
	}
	
	@Override
	public List<Blog> getBlogsByTagOrTitle(List<String> texts) {
		Map<String, List<String>> queries = new HashMap<>();
		queries.put("tags", texts);
		queries.put("title", texts);
		return fetchDocs(queries);
	}
	
	private List<Blog> fetchDocs(Map<String, List<String>> queries){
		List<Criteria> c = new ArrayList<>();
        Query<Blog> query0 = createQuery();
        for (Map.Entry<String, List<String>> entry : queries.entrySet()) {
            Query<Blog> queryC = createQuery();
            String key= entry.getKey();
            List<String> values= entry.getValue();
            for(String value : values) {
            	Criteria cC = queryC.criteria(key).containsIgnoreCase(value);
            	c.add(cC);
            }
        }

        query0.or((Criteria[]) c.toArray(new Criteria[0]));
        return query0.order(ORDER_DESC_CREATED_DATE).asList();
	}
	
	@Override
	public boolean addComment(String id, Comment comment) {
		boolean isSuccess = true;
		Query<Blog> query = createQuery().field("_id").equal(id);
		UpdateOperations<Blog> operations = createUpdateOperations().push("comments",comment,new PushOptions().position(0));
		update(query,operations);
		return isSuccess;
	}

}
