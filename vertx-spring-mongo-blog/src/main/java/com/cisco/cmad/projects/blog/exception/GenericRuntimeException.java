package com.cisco.cmad.projects.blog.exception;

/**
 * @author srihpall
 *
 */
public class GenericRuntimeException extends RuntimeException {

	/**
	 * A UID for serialization.
	 */
	private static final long serialVersionUID = -2579795254860673058L;

	public GenericRuntimeException() {
		super();
	}

	public GenericRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public GenericRuntimeException(String message) {
		super(message);
	}

	public GenericRuntimeException(Throwable cause) {
		super(cause);
	}

}
