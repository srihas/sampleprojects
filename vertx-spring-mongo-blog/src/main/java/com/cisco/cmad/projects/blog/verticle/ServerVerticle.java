package com.cisco.cmad.projects.blog.verticle;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cisco.cmad.projects.blog.model.Blog;
import com.cisco.cmad.projects.blog.model.Comment;
import com.cisco.cmad.projects.blog.model.User;
import com.cisco.cmad.projects.blog.service.BasicService;
import com.cisco.cmad.projects.blog.vertx.VertxUser;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.FaviconHandler;
import io.vertx.ext.web.handler.JWTAuthHandler;
import io.vertx.ext.web.handler.StaticHandler;

@Service
@Qualifier("serverVerticle")
public class ServerVerticle extends AbstractVerticle {
	
	private static final String FAVOURITE_BLOGS = "favouriteBlogs";

	private static final String BLOG_ID = "blogId";

	private static final String USER_INFO = "user.info";

	private static final String SUCCESS = "success";

	private static final String USER_NAME = "userName";

	private static final String SECRET = "secret";

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private BasicService basicService;
	
	@Value("${server.port}")
	private Integer port;

	@Override
	public void start(Future<Void> future) throws Exception {
		logger.info("starting..."+vertx);
		Router router = Router.router(vertx);
		
		router.route().handler(FaviconHandler.create("web/favicon.ico"));
		
		router.route("/about").handler(rctx -> {
			HttpServerResponse response = rctx.response();
			response.putHeader("content-type", "text/html")
					.end("<h1>Hello from my first Vert.x 3 application via routers</h1>");
		});
		
		router.route().handler(BodyHandler.create());
		
		router.route("/resources/js/*").handler(StaticHandler.create("js").setCachingEnabled(false));
		router.route("/resources/css/*").handler(StaticHandler.create("css").setCachingEnabled(false));
		router.route("/resources/fonts/*").handler(StaticHandler.create("fonts").setCachingEnabled(false));
		
		//router.route("/").handler(ctx -> ctx.response().putHeader("location", "/static/login.html").setStatusCode(302).end());
		
		router.route("/main/*").handler(StaticHandler.create("main").setCachingEnabled(false));
		router.route("/").handler(ctx -> ctx.response().putHeader("location", "/main/index.html").setStatusCode(302).end());
		
		router.route("/static/*").handler(StaticHandler.create("web").setCachingEnabled(false));
		
		router.route("/home").handler(ctx -> ctx.response().putHeader("location", "/api/private/index.html").setStatusCode(302).end());
		
		// Authprovider to authenticate based on custom authentication
		AuthProvider authProvider = (authInfo,resultHandler) -> 
			vertx.executeBlocking(fut -> {
				String authInfoString = Json.encode(authInfo);
				vertx.eventBus().send("user.credentials.validate",authInfoString, r -> {
					if("false".equals(r.result().body().toString())) {
						fut.fail("Invalid user");
						return;
					}
					fut.complete(new VertxUser(authInfo));
				});
			    }, resultHandler);
	    
	    router.get("/api/user/available/:userName").handler(rctx -> {
			String userName = rctx.request().getParam(USER_NAME);
			vertx.eventBus().send("user.username.available",userName, r -> rctx.response().setStatusCode(200).end(r.result().body().toString()));
		});
	    
	    router.get("/api/blog/tags").handler(rctx -> {
			String param = rctx.request().getParam("term");
			List<String> tags = new ArrayList<>();
			List<String> tagsComplete = basicService.getTags();
			if(null!=param) {
				for (String tag : tagsComplete) {
					if(StringUtils.startsWithIgnoreCase(tag,param)) {
						tags.add(tag);
					}
				}
			}
			tags = tags.isEmpty()?tagsComplete:tags;
	    	rctx.response().setStatusCode(200).end(Json.encode(tags));
	    });
	    
	    router.post("/api/user/register").handler(rctx -> vertx.eventBus().send("user.register", rctx.getBodyAsJson(), r -> rctx.response().setStatusCode(200).end(r.result().body().toString())));
	    
	    // Create a JWT Auth Provider
	    JWTAuth jwt = JWTAuth.create(vertx, new JsonObject()
	        .put("keyStore", new JsonObject()
	            .put("type", "jceks")
	            .put("path", "keystore.jceks")
	            .put("password", SECRET)));

	    // Handle new login (jwt based)
	    router.route("/login").handler(rctx -> {
	    	HttpServerRequest req = rctx.request();
			JsonObject authInfo = rctx.getBodyAsJson();
	    	authProvider.authenticate(authInfo, res -> {
	    		JsonObject responseJson = new JsonObject();
	    		if (res.succeeded()) {
	    			String token = jwt.generateToken(new JsonObject().put(USER_NAME, authInfo.getString(USER_NAME)), new JWTOptions().setExpiresInSeconds(3600L));
	    			responseJson.put(SUCCESS, true);
	    			responseJson.put("token", token);
	    		} else {
	    			responseJson.put(SUCCESS, false);
	    		}
	    		req.response().end(Json.encode(responseJson));
	    	});
	    });
	    
	    router.route("/api/private/*").handler(StaticHandler.create().setCachingEnabled(false).setWebRoot("private"));
	    
	    router.get("/api/blogs/all").handler(rctx -> {
			vertx.eventBus().send("blog.list",Json.encode(new ArrayList<>()), r1 -> rctx.response().setStatusCode(200).end(r1.result().body().toString()));
		});
	    
	    router.route("/api/*").handler(JWTAuthHandler.create(jwt));
	    
	    router.get("/api/blogs/fav").handler(rctx -> {
	    	if(null!=rctx.user()) {
	    		JsonObject principal = rctx.user().principal();
	    		String userName = principal.getString(USER_NAME);
				vertx.eventBus().send(USER_INFO,userName, r -> {
					User user = Json.decodeValue(r.result().body().toString(), User.class);
					List<String> favouriteBlogs = new ArrayList<>();
					if(null!=user) {
						favouriteBlogs = user.getFavouriteBlogs();
					}
					rctx.response().setStatusCode(200).end(Json.encode(favouriteBlogs));
				});
	    	} else {
	    		rctx.response().setStatusCode(200).end(Json.encode(new ArrayList<>()));
	    	}
		});
	    
	    router.route("/api/token/valid").handler(rctx -> rctx.response().setStatusCode(200).end("true"));
	    
	    router.get("/api/user/info").handler(rctx -> {
	    	JsonObject principal = rctx.user().principal();
    		String userName = principal.getString(USER_NAME);
    		
			vertx.eventBus().send(USER_INFO,userName, r -> {
				User user = Json.decodeValue(r.result().body().toString(), User.class);
				user.setPassword("*****");
				rctx.response().setStatusCode(200).end(Json.encode(user));
			});
		});
	    
	    router.post("/api/user/update").handler(rctx -> {
			JsonObject object = rctx.getBodyAsJson();
			User user = Json.decodeValue(object.toString(),User.class);
			JsonObject principal = rctx.user().principal();
			String userName = principal.getString(USER_NAME);
			vertx.eventBus().send(USER_INFO,userName, r -> {
				User userOfc = Json.decodeValue(r.result().body().toString(), User.class);
				if(!user.getUserName().equals(userOfc.getUserName())) {
					rctx.fail(403);
				} else {
					logger.info("Updating user profile to "+object.toString());
					vertx.eventBus().send("user.update", object, r1 -> rctx.response().setStatusCode(200).end(r1.result().body().toString()));
				}
			});
			
		});
	    
	    /*router.get("/api/blogs/all").handler(rctx -> {
	    	String userName = "srihas";
	    	if(null!=rctx.user()) {
	    		JsonObject principal = rctx.user().principal();
				userName = principal.getString(USER_NAME);
	    	}
		    vertx.eventBus().send(USER_INFO,userName, r -> {
					User user = Json.decodeValue(r.result().body().toString(), User.class);
					List<String> favouriteBlogs = new ArrayList<>(); 
					if(null!=user) {
						favouriteBlogs = user.getFavouriteBlogs();
					}
					vertx.eventBus().send("blog.list",Json.encode(favouriteBlogs), r1 -> rctx.response().setStatusCode(200).end(r1.result().body().toString()));
			});
		});*/
	    
	    router.get("/api/blogs/search").handler(rctx -> {
	    	String query = rctx.request().getParam("query");
			List<String> tags = new ArrayList<>();
			tags.add(query);
			vertx.eventBus().send("blog.search",Json.encode(tags), r1 -> rctx.response().setStatusCode(200).end(r1.result().body().toString()));
		});
	    
	    router.get("/api/blogs/my").handler(rctx -> {
	    	JsonObject principal = rctx.user().principal();
	    	String userName = principal.getString(USER_NAME);
	    		
	    	vertx.eventBus().send(USER_INFO,userName, r -> {
				User user = Json.decodeValue(r.result().body().toString(), User.class);
				JsonObject obj = new JsonObject().put("userName", user.getUserName());
				vertx.eventBus().send("user.blog.list",Json.encode(obj), r1 -> rctx.response().setStatusCode(200).end(r1.result().body().toString()));
			});
		});
	    
	    router.get("/api/blogs/myinterests").handler(rctx -> {
	    	JsonObject principal = rctx.user().principal();
	    	String userName = principal.getString(USER_NAME);
	    	
	    	vertx.eventBus().send(USER_INFO,userName, r -> {
				User user = Json.decodeValue(r.result().body().toString(), User.class);
				List<String> tags = user.getAreaOfInterest();
				JsonObject obj = new JsonObject().put("tags", tags);
				vertx.eventBus().send("blog.list.tags",Json.encode(obj), r1 -> rctx.response().setStatusCode(200).end(r1.result().body().toString()));
			});
		});
	    
	    router.get("/api/blogs/favorites").handler(rctx -> {
	    	JsonObject principal = rctx.user().principal();
			String userName = principal.getString(USER_NAME);
			vertx.eventBus().send(USER_INFO,userName, r -> {
				User user = Json.decodeValue(r.result().body().toString(), User.class);
				vertx.eventBus().send("blog.list.fav",Json.encodePrettily(user), r1 -> rctx.response().setStatusCode(200).end(r1.result().body().toString()));
			});
		});
	    
	    router.post("/api/blog/create").handler(rctx -> {
	    	JsonObject principal = rctx.user().principal();
    		String userName = principal.getString(USER_NAME);
    		
			Blog blog = Json.decodeValue(rctx.getBodyAsString(), Blog.class);
			blog.setUserName(userName);
			
			vertx.eventBus().send("blog.create", Json.encodePrettily(blog), r -> rctx.response().setStatusCode(200).end(r.result().body().toString()));
		});
	    
	    router.post("/api/blog/comment").handler(rctx -> {
			logger.info(rctx.getBodyAsString());
			JsonObject principal = rctx.user().principal();
    		String userName = principal.getString(USER_NAME);
    		
			JsonObject obj = rctx.getBodyAsJson();
			obj.put(USER_NAME, userName);
			vertx.eventBus().send("blog.add.comment", Json.encode(obj), r -> {
				String responseJson = r.result().body().toString();
				JsonObject jsonObject = new JsonObject(responseJson);
				if(SUCCESS.equals(jsonObject.getValue("status"))){
					Comment comment = Json.decodeValue((String)jsonObject.getValue("data"),Comment.class);
					rctx.response().setStatusCode(200).end(Json.encode(comment));
				} else {
					rctx.response().setStatusCode(500).end("Error while adding comment");
				}
			});
		});
	    
	    router.get("/api/blog/view").handler(rctx -> {
	    	JsonObject principal = rctx.user().principal();
    		String userName = principal.getString(USER_NAME);
    		
	    	String blogId = rctx.request().getParam(BLOG_ID);
	    	vertx.eventBus().send(USER_INFO,userName, r -> {
				User user = Json.decodeValue(r.result().body().toString(), User.class);
				JsonObject obj = new JsonObject().put(BLOG_ID, blogId).put(FAVOURITE_BLOGS, user.getFavouriteBlogs());
				vertx.eventBus().send("blog.list.id",Json.encode(obj), r1 -> rctx.response().setStatusCode(200).end(r1.result().body().toString()));
			});
		});
	    
	    router.post("/api/blog/update").handler(rctx -> {
	    	JsonObject jsonObject = rctx.getBodyAsJson();
	    	boolean favorite = jsonObject.getBoolean("favorite");
	    	String blogId = jsonObject.getString(BLOG_ID);
	    	JsonObject principal = rctx.user().principal();
    		String userName = principal.getString(USER_NAME);
    		
	    	JsonObject obj = new JsonObject().put(USER_NAME, userName).put("favorite", favorite);
	    	obj.put(BLOG_ID, blogId);
	    	
	    	vertx.eventBus().send("user.favorites.add.blog", Json.encode(obj), r -> rctx.response().setStatusCode(200).end(r.result().body().toString()));
	    });
	    
		vertx.createHttpServer().requestHandler(router::accept).listen(config().getInteger("http.port", port),
				result -> {
					if (result.succeeded()) {
						future.complete();
					} else {
						future.fail(result.cause());
					}
				});
	}

	@Override
	public void stop() throws Exception {
		logger.info("stopping...");
		super.stop();
	}
	
}
