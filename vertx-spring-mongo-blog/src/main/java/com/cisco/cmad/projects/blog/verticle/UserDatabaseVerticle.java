package com.cisco.cmad.projects.blog.verticle;

import java.security.MessageDigest;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.cisco.cmad.projects.blog.dao.UserDAO;
import com.cisco.cmad.projects.blog.exception.GenericRuntimeException;
import com.cisco.cmad.projects.blog.model.User;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;

@Service
@Qualifier("userDatabaseVerticle")
public class UserDatabaseVerticle extends AbstractVerticle {
	
	private static final String USER_NAME = "userName";

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private Validator validator;
	
	@Autowired
	private UserDAO userDao;
	
	@Override
	public void start() throws Exception {
		
		vertx.eventBus().consumer("user.register", message -> {
			logger.info("Received register request : "+message.body().toString());
			User user = Json.decodeValue(message.body().toString(), User.class);
		    boolean isSuccess = false; 
		    if(isUserDetailsValid(user)) {
				user.setPassword(getHashString(user.getPassword()));
				isSuccess = userDao.createUser(user);
		    } else {
		    	logger.info("Input is not valid");
		    }
			message.reply(isSuccess);
		});
		
		vertx.eventBus().consumer("user.update", message -> {
			logger.info("Received user profile update request : "+message.body().toString());
			User user = Json.decodeValue(message.body().toString(), User.class);

		    boolean isSuccess = false; 
		    if(isUserDetailsValid(user)) {
		    	Optional<User> userOpt = userDao.getUserById(user.getUserName());
		    	
		    	if(userOpt.isPresent()) {
		    		User userOfc = userOpt.get();
		    		updateUser(user, userOfc);
		    		isSuccess = userDao.updateUser(userOfc);
		    	}
		    } else {
		    	logger.error("Input is not valid");
		    }
		    if(isSuccess)
				logger.info("User profile updated successfully");
			message.reply(isSuccess);
		});
		
		vertx.eventBus().consumer("user.info", message -> {
			String userName = message.body().toString();
			logger.info("Getting user info for "+userName);
			Optional<User> userOptional = userDao.getUserById(userName);
			User user = null;
			if(userOptional.isPresent()) {
				user = userOptional.get();
			}
			logger.info("User info : "+user);
			message.reply(Json.encodePrettily(user));
		});
		
		vertx.eventBus().consumer("users.list",message -> {
			logger.info("Getting list of users");
			List<User> users = userDao.getAllUsers();
			logger.info("Users list : "+users);
			message.reply(Json.encodePrettily(users));
		});
		
		vertx.eventBus().consumer("user.username.available", message -> {
			String userName = message.body().toString();
			logger.info("Getting user info for "+userName);
			message.reply(userDao.isUserIdAvailable(userName));
		});
		
		vertx.eventBus().consumer("user.credentials.validate", message -> {
			JsonObject item = new JsonObject(message.body().toString());
			logger.info("Received validation request : "+item);
			String userName = item.getString(USER_NAME);
			boolean isValid = userDao.isValid(userName, getHashString(item.getString("password")));
			message.reply(isValid);
		});
		
		vertx.eventBus().consumer("user.favorites.add.blog", message -> {
			JsonObject item = new JsonObject(message.body().toString());
			logger.info("Received set favorite request : "+item);
			
			String userName = item.getString(USER_NAME);
			String blogId = item.getString("blogId");
			boolean favorite = item.getBoolean("favorite");
			
			boolean isSuccess = userDao.setFavoriteBlog(userName, blogId, favorite);
			
			message.reply(isSuccess);
		});
		
	}

	private void updateUser(User user, User userOfc) {
		userOfc.setFirstName(user.getFirstName());
		userOfc.setLastName(user.getLastName());
		userOfc.setEmail(user.getEmail());
		userOfc.setPhoneNumber(user.getPhoneNumber());
		userOfc.setAreaOfInterest(user.getAreaOfInterest());
		if(!"*****".equals(user.getPassword())) {
			userOfc.setPassword(getHashString(user.getPassword()));
		}
	}

	private boolean isUserDetailsValid(User user) {
		Set<ConstraintViolation<User>> constraintViolations = validator.validate(user);
		Set<String> messages = new HashSet<>(constraintViolations.size());
		messages.addAll(constraintViolations.stream()
		        .map(constraintViolation -> String.format("%s", constraintViolation.getMessage()))
		        .collect(Collectors.toList()));
		logger.info("messages "+messages);
		return constraintViolations.isEmpty();
	}
	
	/**
	 * Hash string.
	 *
	 * @param message the message
	 * @return the string
	 * @throws Exception 
	 * @throws GenericRuntimeException the license exception
	 */
	private String getHashString(String message) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hashedBytes = digest.digest(message.getBytes("UTF-8"));
 
            return convertByteArrayToHexString(hashedBytes);
        } catch (Exception ex) {
            throw new GenericRuntimeException(
                    "Could not generate hash from String", ex);
        }
    }
	
	/**
     * Convert byte array to hex string.
     *
     * @param arrayBytes the array bytes
     * @return the string
     */
    private String convertByteArrayToHexString(byte[] arrayBytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < arrayBytes.length; i++) {
            sb.append(Integer.toString((arrayBytes[i] & 0xff) + 0x100, 16)
                    .substring(1));
        }
        return sb.toString();
    }

}
