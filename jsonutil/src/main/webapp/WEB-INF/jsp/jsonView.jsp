<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<html>
<head>
<title>Json String</title>
</head>
 
<body>
<c:choose>
<c:when test="${isSuccess}">
	<p>Json saved successfully and can be acccessed as <a href="${restUrl}">${fileName}.json</a></p>
	<p>Path is <b>${restUrl}</b></p>
	<p>Json content entered is:</p>
	<p>${jsonString}</p>
</c:when>
<c:otherwise>
	<p>Json entered is invalid and could not be saved:</p>
	<p>${jsonString}</p>
</c:otherwise>
</c:choose>
</body>
 
</html>