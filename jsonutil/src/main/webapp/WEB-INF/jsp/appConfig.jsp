<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Application Configuration</title>
</head>
<body>
<form:form method="POST" commandName="appConfig" action="/rest/config/update">
<c:choose>
	<c:when test="${not isInit}">
		<h2> Application configuration has errors..please fix</h2>
	</c:when>
	<c:otherwise>
		<h2> Application already configured... </h2>
		Click <a href="<%=request.getContextPath()%>">HOME</a>
	</c:otherwise>	
</c:choose>
<br/>
<h2>Configuration</h2>
	<table>
		<thead>
			<tr>
				<th>Name</th>
				<th>Value</th>
				<th>Comments</th>
			</tr>
		</thead>
		<tbody>
		<c:choose>			
		<c:when test="${isInit}">
			<tr>
				<td>Json directory</td>
				<td>${appConfig.jsonDir}</td>
				<td></td>
			</tr>
		</c:when>
		<c:otherwise>
			<tr>
				<td>Json directory</td>
				<td>
					<form:input path="jsonDir" />
				</td>
				<td>
					<p>The directory needs to have write access.</p>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<input type="submit" value="Save"/>
				</td>
			</tr>
		</c:otherwise>
		</c:choose>	
		</tbody>
	</table>
</form:form>
<br/>
<p>Note: Make the changes in app.properties to update json directory across multiple sessions</p>	
</body>
</html>