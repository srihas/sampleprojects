<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<html>
<head>
<title>Json Files</title>
</head>
 
<body>
<c:choose>
<c:when test="${fn:length(jsonFileNames) gt 0 }">
Following are the json files already saved: 
<table>
<c:forEach var="fileName" items="${jsonFileNames}">
	<c:set var="fileUrl" value="${fn:replace(restUrl,'{name}', fileName)}" />
    <tr>
    	<td><p><a href="${fileUrl}">${fileName}.json</a><p></td>
    	<td><p><b>${fileUrl}</b></p></td>
    </tr>	
</c:forEach>
</table>
<br/>
<p><a href="download">Click</a> to download all the files as a zip</p>
</c:when>
<c:otherwise>
No files of type json.
</c:otherwise>
</c:choose>
</body>
 
</html>