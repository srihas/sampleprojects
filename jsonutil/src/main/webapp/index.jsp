<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

Utility to simulate REST calls
<br/>
<ul>
 <li> <a href="/jsonutil/rest/addJson">Add JSON</a>
 <br/>
 <li> <a href="/jsonutil/rest/viewJsonFiles">View saved JSON requests</a>
 <br/>
</ul>
 
