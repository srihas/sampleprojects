package com.cisco.utils.rest.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cisco.utils.core.AppConfigService;
import com.cisco.utils.core.JsonFileFilter;

@Controller
public class JSONController {
	
	private final String CLASSNAME = JSONController.class.getSimpleName();
	
	private final Logger logger = LoggerFactory.getLogger(JSONController.class);
	
	@Autowired
	private AppConfigService appConfigService;
	
	private static final String GETJSON = "getJson/{name}";
	private static final String ADDJSON = "addJson";
	private static final String VIEWJSONFILES = "viewJsonFiles";
	private static final String DOWNLOADFILES = "download";
	
	@RequestMapping(value = ADDJSON, method = RequestMethod.GET)
	public String saveJsonFile(ModelMap model) {
		model.addAttribute("jsonDirectoryPath", appConfigService.getAppConfig().getJsonDir());
		return "addJson";
	}
	
	@RequestMapping(value = GETJSON, method = RequestMethod.GET)
	public @ResponseBody Object getJson(@PathVariable String name) {
		final String METHOD_NAME = ":getJson() ";
		Object val = "";
		try {
			ObjectMapper mapper = new ObjectMapper();
			val = mapper.readValue(new File(appConfigService.getAppConfig().getJsonDir()+name+".json"), Object.class);
			logger.info(CLASSNAME+METHOD_NAME+"Json object is "+val);
		} catch(Exception e) {
			System.err.println("Exception in getting the json. "+e);
		}
		return val;
	}
	

	@RequestMapping(value = "saveJsonFile", method = RequestMethod.POST)
	public String saveJsonFile(@RequestParam("fileName") String fileName,
			@RequestParam("jsonString") String jsonString, ModelMap model, HttpServletRequest request) {
		final String METHOD_NAME = ":saveJsonFile() ";
		logger.info(CLASSNAME+METHOD_NAME+"Json filename :" + fileName);
		logger.info(CLASSNAME+METHOD_NAME+"Json String   :" + jsonString);
		ObjectMapper mapper = new ObjectMapper();
		String response = "";
		boolean isSuccess = false;
		try {
			Object val = mapper.readValue(jsonString, Object.class);
			response = "Json object is :" + val;
			logger.info(CLASSNAME+METHOD_NAME+response);
			isSuccess = true;
			
			mapper.writeValue(new File(appConfigService.getAppConfig().getJsonDir()+fileName+".json"), val);
		} catch (Exception e) {
			response = "Error parsing value";
			System.err.println(response);
			isSuccess = false;
		}
		
		String restUrl = request.getContextPath()+"/rest/"+GETJSON+".json";
		restUrl = restUrl.replace("{name}", fileName);
		logger.info(CLASSNAME+METHOD_NAME+restUrl);
		
		model.addAttribute("restUrl",restUrl);
		model.addAttribute("fileName",fileName);
		model.addAttribute("isSuccess", isSuccess);
		model.addAttribute("response",response);
		model.addAttribute("jsonString",jsonString);
		
		return "jsonView";
	}
	
	@RequestMapping(value = VIEWJSONFILES, method = RequestMethod.GET)
	public String viewJsonFiles(ModelMap model, HttpServletRequest request) {
		final String METHOD_NAME = ":viewJsonFiles() ";
		File dir = new File(appConfigService.getAppConfig().getJsonDir());
		File[] listFiles = dir.listFiles(new JsonFileFilter());
		List<String> jsonFileNames = new ArrayList<String>();
		for(File file:listFiles) {
			//logger.info(CLASSNAME+METHOD_NAME+file.getName());
			String name = file.getName();
			jsonFileNames.add(name.substring(0, name.indexOf(".")));
		}
		logger.info(CLASSNAME+METHOD_NAME+jsonFileNames);
		String restUrl = request.getContextPath()+"/rest/"+GETJSON+".json";
		model.addAttribute("restUrl",restUrl);
		model.addAttribute("jsonFileNames", jsonFileNames);
		return "jsonFilesList";
	}
	
    /**
     * Download jsonfiles.
     *
     * @param response the response
     */
    @RequestMapping(value = DOWNLOADFILES, method = RequestMethod.GET)
    public void downloadJsonfiles(HttpServletResponse response) {
    	final String METHOD_NAME = ":downloadJsonfiles() "; 
    	File dir = new File(appConfigService.getAppConfig().getJsonDir());
		File[] listFiles = dir.listFiles(new JsonFileFilter());
		try {
			ZipOutputStream zos = new ZipOutputStream(response.getOutputStream());
			for(File file:listFiles) {
				logger.info(CLASSNAME+METHOD_NAME+file.getName());
				addToZipFile(file, zos);
			}
			zos.close();
			//response.flushBuffer();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
	
	public void addToZipFile(File file, ZipOutputStream zos) throws FileNotFoundException, IOException {
		FileInputStream fis = new FileInputStream(file);
		ZipEntry zipEntry = new ZipEntry(file.getName());
		zos.putNextEntry(zipEntry);

		byte[] bytes = new byte[1024];
		int length;
		while ((length = fis.read(bytes)) >= 0) {
			zos.write(bytes, 0, length);
		}

		zos.closeEntry();
		fis.close();
	}

}