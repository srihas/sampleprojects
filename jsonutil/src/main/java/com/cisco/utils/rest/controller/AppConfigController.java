package com.cisco.utils.rest.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cisco.utils.core.AppConfig;
import com.cisco.utils.core.AppConfigService;

@Controller
@RequestMapping("config")
public class AppConfigController {

	private final String CLASSNAME = AppConfigController.class.getSimpleName();
	
	private final Logger logger = LoggerFactory.getLogger(AppConfigController.class);
	
	public static final String UPDATECONFIG = "update";

	private static final String GETCONFIG = "get";
	
	@Autowired
	private AppConfigService appConfigService;
	
	@RequestMapping(value = UPDATECONFIG, method = RequestMethod.POST)
	public String updateConfig(ModelMap model,@ModelAttribute AppConfig appConfig,HttpServletRequest request) {
		final String METHOD_NAME = ":updateConfig() ";
		if(model.containsAttribute("fromUri")) {
			logger.info(CLASSNAME+METHOD_NAME+"Redirected from "+model.get("fromUri"));
		}
		logger.info(CLASSNAME+METHOD_NAME+"Updating application configuration to "+appConfig);
		appConfigService.setAppConfig(appConfig);
		model.addAttribute("isInit",appConfigService.getIsInit());
		model.addAttribute("appConfig", appConfigService.getAppConfig());
		return "appConfig";
	}
	
	@RequestMapping(value = GETCONFIG, method = RequestMethod.GET)
	public String getAppConfig(ModelMap model) {
		model.addAttribute("isInit",appConfigService.getIsInit());
		model.addAttribute("appConfig", appConfigService.getAppConfig());
		return "appConfig";
	}
}
