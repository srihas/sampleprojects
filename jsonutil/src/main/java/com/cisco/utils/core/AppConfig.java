package com.cisco.utils.core;

/**
 * The Class AppConfig.
 * 
 * @author srihpall
 */
public class AppConfig {

	/** The json dir. */
	private String jsonDir;
	
	/**
	 * Instantiates a new app config.
	 */
	public AppConfig() {
	}
	
	/**
	 * Instantiates a new app config.
	 *
	 * @param jsonDir the json dir
	 */
	public AppConfig(String jsonDir) {
		this.jsonDir = jsonDir;
	}
	
	/**
	 * Gets the json dir.
	 *
	 * @return the json dir
	 */
	public String getJsonDir() {
		return jsonDir;
	}
	
	/**
	 * Sets the json dir.
	 *
	 * @param jsonDir the new json dir
	 */
	public void setJsonDir(String jsonDir) {
		this.jsonDir = jsonDir;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AppConfig [jsonDir=" + jsonDir + "]";
	}
	
	
}
