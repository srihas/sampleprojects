package com.cisco.utils.core;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * The Class AppConfigService.
 * 
 * @author srihpall
 */
@Service
public class AppConfigService {
	
	private final String CLASSNAME = AppConfigService.class.getSimpleName();
	
	private final Logger logger = LoggerFactory.getLogger(AppConfigService.class);
	
	/** The is init. */
	private boolean isInit;
	
	@Value("${json.dir.path}")
    private String jsonDirectoryPath;
	
	/** The app config. */
	private AppConfig appConfig = null;
	
	/**
	 * Checks if is inits the.
	 *
	 * @return true, if is inits the
	 */
	public boolean getIsInit() {
		final String METHOD_NAME = ":getIsInit() ";
		if(appConfig==null) {
			appConfig = new AppConfig(jsonDirectoryPath);
		}
		if(appConfig != null) {
			String jsonDir = appConfig.getJsonDir();
			logger.info(CLASSNAME+METHOD_NAME+"json dir is "+jsonDir);
			if(!isInit && jsonDir!=null  && new File(jsonDir).isDirectory() && new File(jsonDir).canWrite()) {
				isInit = true;
			}
		}
		logger.info(CLASSNAME+METHOD_NAME+"returning "+isInit);
		return isInit;
	}
	
	/**
	 * Sets the inits the.
	 *
	 * @param isInit the new inits the
	 */
	public void isIsInit(boolean isInit) {
		this.isInit = isInit;
	}
	
	/**
	 * Gets the app config.
	 *
	 * @return the app config
	 */
	public AppConfig getAppConfig() {
		return appConfig;
	}
	
	/**
	 * Sets the app config.
	 *
	 * @param appConfig the new app config
	 */
	public void setAppConfig(AppConfig appConfig) {
		this.appConfig = appConfig;
	}

}
