package com.cisco.utils.core;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;

import com.cisco.utils.rest.controller.AppConfigController;
 
/**
 * The Class AppInterceptor.
 * 
 * @author srihpall
 */
public class AppInterceptor implements HandlerInterceptor  {
	
	/** The classname. */
	private final String CLASSNAME = AppInterceptor.class.getSimpleName();
	
	/** The logger. */
	private final Logger logger = LoggerFactory.getLogger(AppInterceptor.class);
	
	/** The app config service. */
	@Autowired
	private AppConfigService appConfigService;
	
    /* (non-Javadoc)
     * @see org.springframework.web.servlet.HandlerInterceptor#preHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object)
     */
    @Override
    public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler) throws Exception {
    	final String METHOD_NAME = ":preHandle() ";
    	String requestUri = request.getRequestURI();
        logger.info(CLASSNAME+METHOD_NAME+"preHandle called.. for url: "+requestUri); 
        if (!requestUri.contains(AppConfigController.UPDATECONFIG) && !appConfigService.getIsInit()) {
			logger.info(CLASSNAME+METHOD_NAME+"App configuration not initialized");
			// forward to app configuration view
			ModelAndView mav = new ModelAndView("appConfig");
			ModelMap modelMap = mav.getModelMap();
			modelMap.addAttribute("fromUri", requestUri);
			modelMap.addAttribute("isInit", appConfigService.getIsInit());
			modelMap.addAttribute("appConfig",appConfigService.getAppConfig());
			// eventually populate the model
			throw new ModelAndViewDefiningException(mav);
		}
        logger.info(CLASSNAME+METHOD_NAME+"Application configured, hence returning true"); 
        return true;
    }

	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.HandlerInterceptor#afterCompletion(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, java.lang.Exception)
	 */
	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.HandlerInterceptor#postHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, org.springframework.web.servlet.ModelAndView)
	 */
	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}
}