package com.cisco.utils.core;

import java.io.File;
import java.io.FileFilter;

public final class JsonFileFilter implements FileFilter {

	public boolean accept(File pathname) {
		return pathname.getName().endsWith(".json");
	}
}